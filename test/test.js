#!/usr/bin/env node

/* jshint esversion: 8 */
/* global it, xit, describe, before, after, afterEach */

'use strict';

require('chromedriver');

const execSync = require('child_process').execSync,
    expect = require('expect.js'),
    fs = require('fs'),
    path = require('path'),
    { Builder, By, until } = require('selenium-webdriver'),
    { Options } = require('selenium-webdriver/chrome');

describe('Application life cycle test', function () {
    this.timeout(0);

    let browser, app, vault;
    let authenticated_by_oidc = false;
    const LOCATION = process.env.LOCATION || 'test';
    const TIMEOUT = parseInt(process.env.TIMEOUT, 10) || 30000;
    const EXEC_ARGS = { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' };
    const username = process.env.USERNAME, password = process.env.PASSWORD;

    before(function () {
        const chromeOptions = new Options().windowSize({ width: 1280, height: 1024 });
        if (process.env.CI) chromeOptions.addArguments('no-sandbox', 'disable-dev-shm-usage', 'headless');
        browser = new Builder().forBrowser('chrome').setChromeOptions(chromeOptions).build();
        if (!fs.existsSync('./screenshots')) fs.mkdirSync('./screenshots');
    });

    after(function () {
        browser.quit();
    });

    afterEach(async function () {
        if (!process.env.CI || !app) return;

        const currentUrl = await browser.getCurrentUrl();
        if (!currentUrl.includes(app.domain)) return;
        expect(this.currentTest.title).to.be.a('string');

        const screenshotData = await browser.takeScreenshot();
        fs.writeFileSync(`./screenshots/${new Date().getTime()}-${this.currentTest.title.replaceAll(' ', '_')}.png`, screenshotData, 'base64');
    });

    function getAppInfo() {
        const inspect = JSON.parse(execSync('cloudron inspect'));
        app = inspect.apps.filter(function (a) { return a.location === LOCATION || a.location === LOCATION + '2'; })[0];
        expect(app).to.be.an('object');
    }

    async function waitForElement(elem) {
        await browser.wait(until.elementLocated(elem), TIMEOUT);
        await browser.wait(until.elementIsVisible(browser.findElement(elem)), TIMEOUT);
    }

    async function clearCache() {
        await browser.manage().deleteAllCookies();
        await browser.quit();
        browser = null;
        const chromeOptions = new Options().windowSize({ width: 1280, height: 1024 });
        if (process.env.CI) chromeOptions.addArguments('no-sandbox', 'disable-dev-shm-usage', 'headless');
        chromeOptions.addArguments(`--user-data-dir=${await fs.promises.mkdtemp('/tmp/test-')}`); // --profile-directory=Default
        browser = new Builder().forBrowser('chrome').setChromeOptions(chromeOptions).build();
    }

    async function initVault() {
        const out = execSync('cloudron exec --app ' + app.id + ' -- vault operator init -key-shares=1 -key-threshold=1 -format=json', { encoding: 'utf8' });
        // { unseal_keys_b64: [ 'ail569sGe7eVeu7lnhjAULXI79VDjAHhGcA6K/yON7o=' ],
        //   unseal_keys_hex: [ '6a2979ebdb067bb7957aeee59e18c050b5c8efd5438c01e119c03a2bfc8e37ba' ],
        //   unseal_shares: 1,
        //   unseal_threshold: 1,
        //   recovery_keys_b64: [],
        //   recovery_keys_hex: [],
        //   recovery_keys_shares: 5,
        //   recovery_keys_threshold: 3,
        //   root_token: 's.d70U1n15etd4YCIsiU0yRjuA' }
        vault = JSON.parse(out);
        console.dir(vault);
    }

    async function unsealVault() {
        await clearCache();
        await browser.get(`https://${app.fqdn}/ui/vault/unseal`);
        await browser.wait(until.elementLocated(By.xpath('//input[@name="key"]')), TIMEOUT);
        await browser.sleep(2000);
        await browser.findElement(By.xpath('//input[@name="key"]')).sendKeys(vault.unseal_keys_hex[0]);
        await browser.findElement(By.xpath('//span[text()="Unseal"]')).click();
        await browser.wait(until.elementLocated(By.xpath('//div[@class="splash-page-header"]//h1[text()="Sign in to Vault"]')), TIMEOUT);
    }

    async function loginWithRootToken() {
        await browser.get(`https://${app.fqdn}/ui/vault/auth?with=token`);
        await browser.wait(until.elementLocated(By.xpath('//input[@name="token"]')), TIMEOUT);
        await browser.sleep(2000);
        await browser.findElement(By.xpath('//input[@name="token"]')).sendKeys(vault.root_token);
        await browser.findElement(By.xpath('//span[text()="Sign in"]')).click();
        await browser.wait(until.elementLocated(By.xpath('//*[contains(., "Secrets engines")]')), TIMEOUT);
    }

    async function logout() {
        await browser.get(`https://${app.fqdn}/ui/vault/logout`);
        await browser.wait(until.elementLocated(By.xpath('//div[@class="splash-page-header"]//h1[text()="Sign in to Vault"]')), TIMEOUT);
    }

    async function enableOIDC() {
        const out = execSync(`cloudron exec --app ${app.id} -- /app/pkg/enable-oidc.sh ${vault.root_token}`, { encoding: 'utf8' });
        if (!out.includes('OIDC auth enabled')) throw new Error('Could not enable OIDC auth:' + out);
        console.log(out);
    }

    async function loginOIDC() {
        await browser.get(`https://${app.fqdn}/ui/vault/auth?with=oidc`);
        await browser.wait(until.elementLocated(By.xpath('//div[@class="splash-page-header"]//h1[text()="Sign in to Vault"]')), TIMEOUT);
        await browser.sleep(2000);
        await browser.findElement(By.xpath('//button[@id="auth-submit" and contains(., "Sign in with") and contains(., "OIDC Provider")]')).click();
        await browser.sleep(3000);

        if (!authenticated_by_oidc) {
            // handle popup window
            const originalWindowHandle = await browser.getWindowHandle();
            const allWindowHandles = await browser.getAllWindowHandles();
            const newWindowHandle = allWindowHandles.filter((h) => h !== originalWindowHandle).join();

            await browser.switchTo().window(newWindowHandle);

            await waitForElement(By.id('inputUsername'));
            await browser.findElement(By.id('inputUsername')).sendKeys(username);
            await browser.findElement(By.id('inputPassword')).sendKeys(password);
            await browser.findElement(By.id('loginSubmitButton')).click();
            await browser.sleep(2000);

            authenticated_by_oidc = false; // intention change made for CI

            // switch back to the main window
            await browser.switchTo().window(originalWindowHandle);
        }

        await browser.wait(until.elementLocated(By.xpath('//*[contains(., "Secrets engines")]')), TIMEOUT);
    }

    xit('build app', function () { execSync('cloudron build', EXEC_ARGS); });
    it('install app', function () { execSync('cloudron install --location ' + LOCATION, EXEC_ARGS); });

    it('can get app information', getAppInfo);
    it('init vault', initVault);
    it('unseal vault', unsealVault);
    it('can sign in', loginWithRootToken);
    it('can logout', logout);
    it('enable oidc', enableOIDC);
    it('can oidc login', loginOIDC);
    it('can logout', logout);

    it('restart app', function () { execSync('cloudron restart --app ' + app.id, EXEC_ARGS); });
    it('unseal vault', unsealVault);
    it('can sign in', loginWithRootToken);
    it('can logout', logout);
    it('enable oidc', enableOIDC);
    it('can oidc login', loginOIDC);
    it('can logout', logout);

    it('backup app', function () { execSync('cloudron backup create --app ' + app.id, EXEC_ARGS); });

    it('restore app', async function () {
        await clearCache();
        const backups = JSON.parse(execSync('cloudron backup list --raw'));
        execSync('cloudron uninstall --app ' + app.id, EXEC_ARGS);
        execSync('cloudron install --location ' + LOCATION, EXEC_ARGS);
        getAppInfo();
        await clearCache();
        execSync(`cloudron restore --backup ${backups[0].id} --app ${app.id}`, EXEC_ARGS);
    });

    it('unseal vault', unsealVault);
    it('can sign in', loginWithRootToken);
    it('can logout', logout);
    it('enable oidc', enableOIDC);
    it('can oidc login', loginOIDC);
    it('can logout', logout);

    it('move to different location', function () {
        execSync('cloudron configure --location ' + LOCATION + '2 --app ' + app.id, EXEC_ARGS);
        const inspect = JSON.parse(execSync('cloudron inspect'));
        app = inspect.apps.filter(function (a) { return a.location === LOCATION + '2'; })[0];
        expect(app).to.be.an('object');
    });
    it('unseal vault', unsealVault);
    it('can sign in', loginWithRootToken);
    it('can logout', logout);
    it('enable oidc', enableOIDC);
    it('can oidc login', loginOIDC);
    it('can logout', logout);

    it('uninstall app', function () {
        execSync('cloudron uninstall --app ' + app.id, EXEC_ARGS);
    });

    // test update
    it('can install app', function () { execSync('cloudron install --appstore-id io.vaultproject.cloudronapp2 --location ' + LOCATION, EXEC_ARGS); });
    it('can get app information', getAppInfo);
    it('init vault', initVault);

    it('can update', function () {
        execSync('cloudron update --app ' + app.id, EXEC_ARGS);
        const inspect = JSON.parse(execSync('cloudron inspect'));
        app = inspect.apps.filter(function (a) { return a.location === LOCATION; })[0];
        expect(app).to.be.an('object');
    });
    it('unseal vault', unsealVault);
    it('can sign in', loginWithRootToken);
    it('can logout', logout);
    it('enable oidc', enableOIDC);
    it('can oidc login', loginOIDC);
    it('can logout', logout);

    it('uninstall app', function () {
        execSync('cloudron uninstall --app ' + app.id, EXEC_ARGS);
    });
});
