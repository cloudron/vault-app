FROM cloudron/base:5.0.0@sha256:04fd70dbd8ad6149c19de39e35718e024417c3e01dc9c6637eaf4a41ec4e596c

RUN apt-get update && \
    apt-get install -y libcap2-bin && \
    rm -rf /var/cache/apt /var/lib/apt/lists

# throwing ourselves into said cool directory
WORKDIR /app/code
RUN mkdir -p /app/pkg /app/code

# renovate: datasource=github-releases depName=hashicorp/vault versioning=semver extractVersion=^v(?<version>.+)$
ARG VAULT_VERSION=1.19.0

RUN wget https://releases.hashicorp.com/vault/${VAULT_VERSION}/vault_${VAULT_VERSION}_linux_amd64.zip  && \
    unzip vault_${VAULT_VERSION}_linux_amd64.zip -d /app/code && \
    rm -f /app/code/vault_${VAULT_VERSION}_linux_amd64.zip

# set file caps so the executable can run mlock as non privileged user (https://github.com/hashicorp/vault/issues/122)
RUN setcap cap_ipc_lock=+ep /app/code/vault

ENV PATH=/app/code:$PATH

# these are used the vault CLI tools (-address)
ENV VAULT_ADDR="http://127.0.0.1:8200"
ENV VAULT_API_ADDR="http://127.0.0.1:8200"

# copy start script and OIDC config
COPY start.sh enable-oidc.sh /app/pkg/

# kicking off the start script
CMD [ "/app/pkg/start.sh" ]
