#!/bin/bash
set -eu

[[ -z "${CLOUDRON_OIDC_ISSUER:-}" ]] && echo "Cloudron OIDC is not enabled"

if [[ $# -ne 1 ]]; then
    echo "usage: enable-oidc.sh <root-token>"
    exit 1
fi

# the vault login stashes the raw root token in $HOME/.vault-token
rm -rf /tmp/vault/home && mkdir -p /tmp/vault/home
export HOME=/tmp/vault/home

root_token=$1
echo $root_token | vault login -

# must disable oidc to overwrite the config
vault auth disable oidc
vault auth enable oidc
vault write auth/oidc/config \
    oidc_discovery_url="${CLOUDRON_OIDC_ISSUER}" \
    oidc_client_id="${CLOUDRON_OIDC_CLIENT_ID}" \
    oidc_client_secret="${CLOUDRON_OIDC_CLIENT_SECRET}" \
    default_role="cloudron"
#    allowed_redirect_uris="http://localhost:8200/oidc/callback" \
 
vault write auth/oidc/role/cloudron \
    user_claim="sub" \
    allowed_redirect_uris="${CLOUDRON_APP_ORIGIN}/ui/vault/auth/oidc/oidc/callback"  \
    allowed_redirect_uris="${CLOUDRON_APP_ORIGIN}/oidc/oidc/callback"  \
    policies="webapps" \
    oidc_scopes="openid profile email"

echo "OIDC auth enabled"

